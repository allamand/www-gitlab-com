---
layout: job_page
title: "Sales Director"
---


## Responsibilities

* The same responsibilities apply as for Senior [Account Managers](https://about.gitlab.com/jobs/account-manager).
* Additionally, a Sales Director is responsible for (growth of) sales in specific region, including:
   * Recruiting and coaching new Account Managers
