---
layout: markdown_page
title: "GitLab Primer"
---

Read the pages below to learn more about GitLab:

1. [About](https://about.gitlab.com/about/)
1. [Strategy](https://about.gitlab.com/strategy/#sequence)
1. [Direction](https://about.gitlab.com/direction/#scope)
1. [Handbook](https://about.gitlab.com/handbook/)
1. [Team](https://about.gitlab.com/team/)
1. [Features](https://about.gitlab.com/features/#compare)
1. [Pricing](https://about.gitlab.com/pricing/)
1. [Team](https://about.gitlab.com/team/)
1. [In 13 minutes from Kubernetes to a complete application development tool (VIDEO)](https://about.gitlab.com/2016/11/14/idea-to-production/)
