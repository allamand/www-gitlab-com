---
layout: markdown_page
title: CEO Preferences
---

- TOC
{:toc}

## Intro
{: #intro}

This page is intended to be helpful, feel free to deviate from it and update this page if you think it makes sense.
If there are things that might seem pretentious or overbearing please raise them so we can remove or adapt them.
Many items further down this page are a guideline for our [Executive Assistants](https://about.gitlab.com/jobs/executive-assistant/) (EAs).

## Flaws
{: #flaws}

Transparency and directness are part of our [values](https://about.gitlab.com/handbook/#values) and I want to live them by sharing the flaws I know I have.
I'm fully responsible for improving the things below, listing them is no excuse.
They are listed here for two reasons.
The fist one is so that people know it is not them but my fault.
The second one is so I can improve, I hope that listing them lets people know I appreciate when people speak up about them.

1. I look serious all the time, it is OK to say 'maybe you can smile more'.
2. I love debating, it is OK to say 'please stop debating and start collaborating'.
3. My English pronunciation, choice of words, and grammar are not great. I'm taking lessons but I welcome corrections when we're having a 1:1 conversation and/or when it might confuse people.
4. When in a rush I will jump to conclusions, it is OK to ask 'can we take more time to discuss this'.
5. I sometimes make reports feel like I'm scolding them, as in being angry for a percieved fault. It is OK to say, I don't mind you making that point but your tone doesn't make me feel respected.
6. In my feedback I sometimes sound more like I'm giving an order instead of offering a suggestion, even when I mean the latter. It is OK to say 'that sounds like an order, I would have appreciated it more in the form of a suggestion'.

If you speak up about them I should thank you for it, it is OK to say 'this was on your list of flaws so I kinda expected a thank you'.
I'm sure I have more flaws that affect my professional life.
Feel free to send a merge request to add them or communicate them anonymously to People Operations so they can send a merge request.

## Communication
{: #communication}

Thanks to [Mårten Mickos](https://www.linkedin.com/in/martenmickos) for the inspiration for this section. All good idea's are his, all bad ones mine.

I am a visual person much more than auditory, and I am a top-down person much more than bottom-up. This means that I love written communication: issues, email, Google Docs, and chat. Feel free to send me as many emails and chat messages as you like, and about whatever topics you like.

If you have a great new idea or suggestion for me, I appreciate if you can convey it in a picture or in written words, because I learn by seeing more than I learn by hearing. I don't mind if you send me or point me to plans that are in draft mode or not ready. I am happy if I can give useful feedback early. It doesn’t have to be perfect and polished when presented to me.

In written communication, I appreciate the top-down approach. Set the subject header to something descriptive. Start the email by telling me what the email is about. Only then go into details. Don't mix separate topics in the same email, it is perfectly fine to send two emails at almost the same time. Try to have a concrete proposal so I can just reply with OK if that is possible.

I get many email on which I am only cc'd on, I would very much appreciate if you started emails intended specifically for me with "Sid," or some other salutation that makes it clear that the message is for me.

I have accounts on [LinkedIn](https://www.linkedin.com/in/sijbrandij) and [Facebook](https://www.facebook.com/sytse). I will not send invites to team members on those networks since as the CEO I don't want to impose myself on anyone. But I would love to connect and I will happily accept your LinkedIn and Facebook friend request. You can also find me on Twitter as [@sytses](https://twitter.com/sytses).

## Pick your brain meetings
{: #pick-your-brain-meetings}

If people want advice on open source, remote work, or other things related to GitLab we'll consider that. If Sid approves of the request we send the following email:

We would love to help but we want to make sure the content is radiated as wide as possible. Can we do the following?

1. We schedule a 50 minute Zoom video call that is recorded by us, uploaded to Youtube as a private video, and shared with you.
1. You write up a draft post within 48 hours after the interview in a Google Doc and share that with us with suggestion or edit rights for us.
1. You can redact anything you don't want to publish.
1. Our marketing department will work to publish the post.
1. A great examples of this in action are the first two times we did this [https://about.gitlab.com/2016/07/14/building-an-open-source-company-interview-with-gitlabs-ceo/](https://news.ycombinator.com/item?id=12615723) and [https://news.ycombinator.com/item?id=12615723](https://news.ycombinator.com/item?id=12615723). Both got to nr. 1 on [Hacker News](https://news.ycombinator.com/).

The EA should follow up to make sure the draft post is submitted.

## Email
{: #emails}

* Labels: /archive, /respond or /urgent-important
* Prepare draft responses
* Standard reply for recruiters:
“We do not accept solicitations by recruiters, recruiting agencies, headhunters, and outsourcing organizations. Please find all info [on our jobs page](https://about.gitlab.com/jobs/#no-recruiters)”

## Scheduling
{: #scheduling}

* [everytimezone.com](http://www.everytimezone.com) can help determine the best time to schedule
* You can add other [calendars](calendar.google.com) on the left, to see when GitLab team members are free to schedule a meeting with
* The [agenda](https://docs.google.com/document/d/187Q355Q4IvrJ-uayVamoQmh0aXZ6eixAOE90jZspAY4/edit?ts=574610db&pli=1) of items to be handled by the EA for the CEO
* Use for example a tool like [Hipmunk](www.hipmunk.com) to find different flight options with most airlines when needing to book travel
* Keep 1 hour open in calendar per day for email
* Schedule calls in European timezones in the am Pacific (Daylight) Time and US time zones in the pm Pacific (Daylight) Time.
    * MTG |  for meetings in person, either at the "office" or another location
    * INTERVIEW |  for interviews (looping in our PR partner)
    Make sure to block 10 min before for preparations and 10 min after for notes for Sid and send out https://about.gitlab.com/primer/ for background information for the interviewer.
    * CALL | for phone calls
    * VIDEOCALL | for video conference calls using Google Hangout. In case it's a larger list of attendees or when we want to record the call we schedule with Zoom. Make sure to take out the Google Hangout link when you switch to Zoom.
    add message:
    “Please use the link in this invite to join the Google Hangout **OR** Zoom. It will prompt a request for access.
	    * Calls in the hiring process also have:
	    “Please fill out [this form](https://docs.google.com/a/gitlab.com/forms/d/1lBq_oXaqpQRs-SeEs3EvpxFGK55Enqn_nzkLq2l3Rwg/viewform) to discuss during the call”

* Make sure to add any relevant details in the invite.
* Examples for scheduling:
“CALL | Kirsten Abma for Executive Assistant”
“INTERVIEW | Kirsten Abma (TechCrunch)”
“MTG | Bruce Armstrong (Khosla) & Sid Sijbrandij (GitLab) @ [office/location]”
Or for example “Dinner/Lunch @ Bar Agricole - Kirsten Abma & Sid Sijbrandij”

For all external meetings include [visiting info](about.gitlab.com/visiting) (for in-person meetings), our [primer page](about.gitlab.com/primer), phone numbers, and where to find the Hangout URL. Make sure to add both a video call link (Hangout or Zoom) and physical phone numbers.
For meetings or lunch/dinner appointments, always make sure to add the address in the invite of the location where it’s scheduled and make sure to plan travel time (in a separate calendar item, just for Sid) before and after the meeting in case another meeting or call should follow.
Favorite restaurants of Sids are: [Basil Thai in SoMa](http://www.basilthai.com/home.html) for lunch, [Heirloom Cafe](https://heirloom-sf.com/) for dinner and [Odd Job](http://oddjobsf.com/) for drinks.

## Hiring process
{: #hiring-process}

* Hiring managers will tag EA to schedule a call for the applicant with Sid
* Template email with wording to schedule “Kirsten schedule Sid”
* Add date/time and make sure to include timezones
* Add responses of [form](https://docs.google.com/a/gitlab.com/forms/d/1lBq_oXaqpQRs-SeEs3EvpxFGK55Enqn_nzkLq2l3Rwg/viewform) to our ATS; [Workable](https://gitlab.workable.com/backend)

## Travel
{: #travel}

EA does research for the best option for a flight and propose this to Sid before booking.
Current preferences for flights are:
* Aisle seat
* Check a bag for all trips longer than one night
* Frequent Flyer details of all (previously flown) airlines are in EA vault of 1Password

## Mail
{: #mail}

* Check all incoming (physical) mail at 1233 Howard st and sort the urgent and important letters.
* Inform AP if invoices came in
* Inform other people/departments if mail is addressed for them and include a scan of the document

## Expensify
{: #expensify}

* When you’re logged in, you can find wingman account access for Sid in the top right corner menu.
* Check Sids email, using the search bar in the top, to find any receipts for the postings in the current expense report.
* Write down what receipts are missing and email to request them
